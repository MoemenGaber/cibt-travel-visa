<?php
get_header();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    queryForVisaRequirements();
    ?>
    <body>
    <?php
    if (have_rows('requirements_page_data')):
        ?>
        <div class="china-visa">
            <!-- Start Container  -->
            <div class="container">
                <?php while (have_rows('requirements_page_data')):
                    the_row(); ?>
                    <?php if (get_row_layout() == 'visa_and_destination_info_section') { ?>
                    <div class="visa-required">
                        <div class="make-flex">
                            <div class="flex-parent">
                                <h3><?php the_sub_field('visa_title'); ?>
                                    <span><?php the_sub_field('start_your_visa_request_now'); ?></span>
                                </h3>
                                <?php if (have_rows('destination_and_visa_info')): while (have_rows('destination_and_visa_info')): the_row(); ?>
                                    <p><strong><?php the_sub_field('destination_visa_info'); ?>:</strong> <?php the_sub_field('visa_info'); ?> <a href="#">edit</a></p>
                                <?php endwhile; endif; ?>
                                <div class="links">
                                    <ul>
                                        <li><span><i class="far fa-eye"></i></span><a href="#">View Application Kit</a></li>
                                        <li><span><i class="fas fa-envelope"></i></span><a href="#">Email Application Kit</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="flex-parent">
                                <a class="red-button" href="#">Get My Visa <i class="fa fa-chevron-right" aria-hidden="true" style="padding-left:5px;"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- End visa-required -->
                <?php } ?>
                    <?php if (get_row_layout() == 'requirements_boxes_section') { ?>
                    <h1>The Information You Will Need For Your Visa</h1>

                    <div class="information">
                        <ul class="make-flex">
                            <!-- Flex-Child -->
                            <?php if (have_rows('requirement_box')):
                                while (have_rows('requirement_box')):
                                    the_row();
                                    ?>
                                    <li class="child-flex">
                                        <h5><?php the_sub_field('requirement_title'); ?></h5>
                                        <img src="<?php the_sub_field('requirement_image'); ?>">
                                        <div class="show">
                                            <div class="learn-more">
                                                <p>Learn More <i class="fa fa-chevron-down" aria-hidden="true" style="padding-left:5px;"></i></p>
                                            </div>
                                            <div class="show-hide">
                                                <h6><?php the_sub_field('requirement_title'); ?></h6>
                                                <?php the_sub_field('requirement_description_floating_window'); ?>
                                            </div>
                                        </div>
                                    </li>
                                <?php endwhile; endif; ?>
                        </ul>
                    </div>
                    <!-- End information -->
                <?php } ?>
                    <?php if (get_row_layout() == 'consular_closings'): ?>
                    <div class="consular-closings">
                        <h1>Consular Closings</h1>
                        <hr>
                        <?php the_sub_field('consolar_description') ?>
                        <?php if (have_rows('service_fees_section')): ?>
                            <h1>Service Fees</h1>
                            <hr>
                            <?php while (have_rows('service_fees_section')): the_row(); ?>
                                <strong><?php the_sub_field('fees_title'); ?></strong>
                                <p> <?php the_sub_field('fees_description'); ?></p>
                                <?php if (have_rows('fee_line')): ?>
                                    <ul>
                                        <?php while (have_rows('fee_line')): ?>
                                            <?php the_row(); ?>
                                            <li>
                                                <p><?php the_sub_field('fee_title'); ?></p>
                                                <p><?php the_sub_field('fee_price'); ?></p>
                                            </li>
                                        <?php endwhile; ?>
                                    </ul>
                                <?php endif; ?>
                            <?php endwhile; endif; ?>

                        <div class="get-my-visa">
                            <ul>
                                <li><span><i class="far fa-eye"></i></span><a href="#">View Application Kit</a></li>
                                <li><span><i class="fas fa-envelope"></i></span><a href="#">Email Application Kit</a></li>
                            </ul>
                            <a class="visa-button" href="#">Get My Visa <i class="fa fa-chevron-right" aria-hidden="true" style="padding-left:5px;"></i></a>
                        </div>

                    </div>
                <?php endif; ?>
                <?php endwhile; ?>
            </div>
        </div>
    <?php endif; ?>
    </body>
    <?php

    wp_reset_query();

}else{
    wp_redirect(home_url());
}
            get_footer();

