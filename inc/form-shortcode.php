<?php
function visaCIBTFormShortcode() {
	?>
    <?php
	global $wpdb;
	$page_name='requirements';
	if ( null === $wpdb->get_row( "SELECT post_name FROM wp_posts WHERE post_name = '" . $page_name . "'", 'ARRAY_A' ) ) {
	    $requirements_page_path=get_home_url();
	}else{
		$requirements_page_path=get_permalink(get_page_by_path('requirements'));
    }
		?>
    <form method="POST" action="<?php echo $requirements_page_path; ?>">
        <div>
            <label>I Hold passport from:</label>
			<?php CategoriesMenuFormQuery( 'Countries' ); ?>
        </div>
        <div class="load-state">
            <label>State of Residence:</label>
            <select name="states" class="states">
                <option>Please select a country</option>
            </select>
        </div>
        <div class="Destination-selection">
            <label>I am going to:</label>
			<?php CategoriesMenuFormQuery( 'Destination' ); ?>
        </div>
        <div class="purpose-selection">
            <label>My purpose of trip is:</label>
		    <?php CategoriesMenuFormQuery( 'Purpose' ); ?>
        </div>

        <input type="submit" class="btn" value="Submit">
    </form>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url( "admin-ajax.php" ); ?>';
        jQuery(function (jQuery) {
            jQuery('body').on('change', '.country', function () {
                var country = jQuery(this).val();
                if (country != '') {
                    var data = {
                        action: 'get_states_by_ajax',
                        country: country,
                        'security': '<?php echo wp_create_nonce( "get-states" ); ?>'
                    };

                    jQuery.post(ajaxurl, data, function (response) {
                        jQuery('.load-state').html(response);
                    });
                } else {
                    alert('not true');
                }
            });
        });
    </script>
	<?php

}

add_shortcode( 'visarequirementsform', 'visaCIBTFormShortcode' );