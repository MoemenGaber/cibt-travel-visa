<?php
/*
 * Plugin Name: CIBT-Travel Visa
 * Description: This is a simple plugin to make you able to link destination with resident & generate a custom page with custom data easily, just copy the shortcode '<code>[visarequirementsform]</code>'.
 * Author: You Know me.
 * Plugin URI: #
 * Version : 1.1
 */

$dir = plugin_dir_path( __FILE__ );

require $dir . '/inc/cibt-post-types.php';
require $dir . '/inc/functions.php';
require $dir . '/inc/form-shortcode.php';


// Creating a new page for Results after activating our plugin for the first time!
function add_cibt_results_page() {
	if ( ! current_user_can( 'activate_plugins' ) ) {
		return;
	}
	global $wpdb;
	$page_name = 'requirements';
	if ( null === $wpdb->get_row( "SELECT post_name FROM wp_posts WHERE post_name = '" . $page_name . "'", 'ARRAY_A' ) ) {

		$current_user = wp_get_current_user();

		// create post object
		$page = array(
			'post_title'  => __( 'Requirements' ),
			'post_status' => 'publish',
			'post_author' => $current_user->ID,
			'post_type'   => 'page',
		);

		// insert the post into the database
		wp_insert_post( $page );
	}
}
register_activation_hook( __FILE__, 'add_cibt_results_page' );
// force use of templates from plugin folder

function cpte_force_template( $template )
{
	$dir = plugin_dir_path( __FILE__ );

	if( is_page( 'requirements' ) ) {
		$template =  $dir.'/page-requiremenets.php';
	}
	return $template;
}
add_filter( 'template_include', 'cpte_force_template' );


// Define path and URL to the ACF plugin.
define( 'MY_ACF_PATH', $dir.'/inc/acf/' );
// Include the ACF plugin.
include_once( MY_ACF_PATH . 'acf.php' );
// Our Custom plugin fields
include_once $dir . '/inc/acf-fields.php';

